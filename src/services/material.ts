import type { Stock } from "@/types/Stock";
import http from "./http";


function addMaterial(stock: Stock){
   return  http.post('/Material', stock)
}

function updateMaterial(stock: Stock){
    return  http.patch(`/Material/${stock.id}`, stock)
 }

 function delMaterial(stock: Stock){
    return  http.delete(`/Material/${stock.id}`)
 }

 function getMaterial(id: number){//fix
    return  http.get(`/Material/${id}`)
 }

 function getMaterials(){
    return  http.get('/Material')
 }

 export default { addMaterial, updateMaterial, delMaterial, getMaterial, getMaterials}