import type { Member } from './Member'
import type { ReceiptItem } from './ReceiptItem'
import type { User } from './User'

type Receipt = {
  id: number
  createdDate: Date
  createdDateString?: String
  totolBefor: number
  memberDiscount: number
  total: number
  receivedAmount: number
  change: number
  paymentType: string
  userId: number
  user?: User
  memberId: number
  member?: Member
  receiptItem?: ReceiptItem[]
}

export type { Receipt }
