import type { Stock } from './Stock'
type StockItem = {
    id: number
    name: string
    unit: number
    price:number
    stockId:number
    stock:Stock
}

export { type StockItem }