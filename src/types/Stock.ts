type Stock = {
    id: number
    name: string
    quantityLasts:number
    quantityNow:number
    quantityIm: number
    MinQuantity: number
    unit: string
    price:number
}

export { type Stock }