import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import type { Member } from '@/types/Member'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import { useDateStore } from './date'

export const useReceiptStore = defineStore('receipt', () => {
  const memberStore = useMemberStore()
  const receiptDialog = ref(false)
  const receiptDialogDetail = ref(false)
  const authStore = useAuthStore()
  const dateStore = useDateStore()
  const selectedPromotion = ref('Select Promotion')
  const DiscountPromotion = ref(0.0)
  const savedReceipts = ref<Receipt[]>([])
  const receipt = ref<Receipt>({
    id: 0,
    createdDate: new Date(),
    createdDateString: '',
    totolBefor: 0,
    memberDiscount: 0,
    total: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: 'Select Payment',
    userId: authStore.currentUser.id,
    user: authStore.currentUser,
    memberId: memberStore.currentMember ? memberStore.currentMember.id : 0,
    member: memberStore.currentMember || undefined
  })
  const receiptItems = ref<ReceiptItem[]>([])
  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    } else {
      const newReceipt: ReceiptItem = {
        id: -1,
        name: product.name,
        price: product.price,
        unit: 1,
        Sweetnesslevel: '100%',
        productId: product.id,
        product: product
      }
      receiptItems.value.push(newReceipt)
      calReceipt()
    }
  }
  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }
  const memberdis = ref(0)
  const prodis = ref(0)
  const discount = ref(0)

  function calReceipt() {
    let totolBefor = 0
    const memberDiscount = 0.9
    receipt.value.memberDiscount += 0

    for (const item of receiptItems.value) {
      totolBefor = totolBefor + item.price * item.unit
    }

    receipt.value.totolBefor = totolBefor

    if (memberStore.currentMember) {
      const localMemberDis = Math.round(totolBefor * (1.0 - memberDiscount))
      receipt.value.memberDiscount = localMemberDis
      memberdis.value = localMemberDis
      receipt.value.memberDiscount = memberdis.value + prodis.value
      receipt.value.total = Math.round(receipt.value.totolBefor - receipt.value.memberDiscount)
    } else {
      receipt.value.memberDiscount = 0
      receipt.value.total = totolBefor
    }
  }

  function updatePromotion(selectedPromotion: string) {
    console.log('Select Promotion:', selectedPromotion)
    receipt.value.memberDiscount += 0

    if (selectedPromotion === 'Happy New Year') {
      console.log('Selected Happy New Year')
      DiscountPromotion.value = 0.7
    } else if (selectedPromotion === 'Summer Season') {
      console.log('Selected Summer Season')
      DiscountPromotion.value = 0.8
    } else if (selectedPromotion === 'Winter Season') {
      console.log('Selected Winter Season')
      DiscountPromotion.value = 0.9
    } else if (selectedPromotion === 'Select Promotion') {
      DiscountPromotion.value = 1.0
    }

    const localMemberDis = Math.round(receipt.value.totolBefor * (1.0 - DiscountPromotion.value))
    receipt.value.memberDiscount = localMemberDis
    prodis.value = localMemberDis
    receipt.value.memberDiscount = memberdis.value + prodis.value
    receipt.value.total = Math.round(receipt.value.totolBefor - receipt.value.memberDiscount)
  }

  function clearDiscount() {
    memberdis.value = 0
    prodis.value = 0
  }

  function calReceivedAmount(Amount: number) {
    receipt.value.receivedAmount = Amount
    receipt.value.change = receipt.value.receivedAmount - receipt.value.total
  }

  // const selectedPayment = ref('Selected Payment')

  function updatePaymentType(selectedPayment: string) {
    console.log('Selected Payment:', selectedPayment)
    const Amount = 0
    receipt.value.paymentType = selectedPayment
    console.log('Updated Payment Type:', receipt.value.paymentType)

    if (receipt.value.paymentType === 'Cash') {
      console.log('Selected Cash')
      receipt.value.receivedAmount = Amount
      receipt.value.change = receipt.value.receivedAmount - receipt.value.total
    } else if (receipt.value.paymentType === 'PromptPay') {
      console.log('Selected PromptPay')
      receipt.value.receivedAmount = receipt.value.total
    } else if (receipt.value.paymentType === 'Credit') {
      console.log('Selected Credit')
      receipt.value.receivedAmount = receipt.value.total
    }
    receipt.value.change = receipt.value.receivedAmount - receipt.value.total
  }

  // function updateSweetness(item: ReceiptItem, sweetnessLevel: string): void {
  //   item.sweet = sweetnessLevel
  // }

  const CheckSaveStatus = ref(false)
  function CheckSave() {
    CheckSaveStatus.value = true
    receiptDialog.value = false
    saveReceipt()
  }

  function showReceiptDialog() {
    changeTime()
    receipt.value.user = authStore.currentUser
    receipt.value.receiptItem = receiptItems.value
    receiptDialog.value = true
  }

  function clearDate() {
    console.log('Befor = ' + receipt.value.createdDateString)
    dateStore.clear()
    receipt.value.createdDate = new Date()
    receipt.value.createdDateString = formatDateTime(receipt.value.createdDate)
    console.log('After = ' + receipt.value.createdDateString)
  }
  function changeTime() {
    receipt.value.createdDateString = formatDateTime(receipt.value.createdDate)
  }
  function formatDateTime(date: Date): string {
    return date.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      hour12: false
    })
  }

  const currentStep = ref(1)

  const nextStep = () => {
    if (receiptItems.value.length > 0) {
      currentStep.value++
    } else {
      alert('Please select at least one product before proceeding to the next step.')
    }
  }

  const prevStep = () => {
    if (currentStep.value > 1) {
      currentStep.value--
    }
  }

  let lastUsedId = 0

  if (savedReceipts.value.length > 0) {
    lastUsedId = Math.max(...savedReceipts.value.map((receipt) => receipt.id))
  }

  function updateCurrentMember(member: Member | null) {
    const memberId = member ? member.id : 0

    receipt.value.memberId = memberId || Math.max(lastUsedId, 1)

    receipt.value.member = member || { id: receipt.value.memberId, name: '-', tel: '-' }
  }

  function saveReceiptDataToStore() {
    if (isNaN(receipt.value.change) || receipt.value.change < 0) {
      alert('Please check the amount is correct.')
    } else {
      if (
        receipt.value.paymentType === '' ||
        receipt.value.paymentType === 'undefined' ||
        receipt.value.paymentType === 'Select Payment'
      ) {
        alert('Please choose a payment')
      } else {
        // calReceipt()
        showReceiptDialog()
        clearDate()
      }
    }
  }

  function saveReceipt() {
    const savedReceipt = { ...receipt.value }

    savedReceipt.receiptItem = receiptItems.value

    if (
      // savedReceipt.user?.fullName !== '' ||
      savedReceipt.totolBefor !== 0 ||
      savedReceipt.memberDiscount !== 0 ||
      savedReceipt.total !== 0
    ) {
      lastUsedId++
      savedReceipt.id = lastUsedId
      savedReceipts.value.push(savedReceipt)
      console.log('Receipt data saved to store:', savedReceipt)
    } else {
      console.log(
        'Receipt data not saved to store as totolBefor, memberDiscount, and total are all 0.'
      )
    }
  }
  function loadReceiptDetail(receiptId: number) {
    console.log(receiptId)

    const selectedReceipt = savedReceipts.value.find((receipt) => receipt.id === receiptId)

    if (selectedReceipt) {
      const mockedReceipt: Receipt = {
        id: selectedReceipt.id,
        createdDate: new Date(selectedReceipt.createdDate),
        totolBefor: selectedReceipt.totolBefor,
        memberDiscount: selectedReceipt.memberDiscount,
        total: selectedReceipt.total,
        receivedAmount: selectedReceipt.receivedAmount,
        change: selectedReceipt.change,
        paymentType: selectedReceipt.paymentType,
        receiptItem: selectedReceipt.receiptItem || [],
        createdDateString: selectedReceipt.createdDateString,
        userId: selectedReceipt.userId,
        user: selectedReceipt.user,
        memberId: selectedReceipt.memberId
      }

      receipt.value = mockedReceipt
      receiptDialogDetail.value = true
    } else {
      console.error(`Receipt with id ${receiptId} not found in savedReceipts`)
    }
  }

  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      createdDateString: '',
      totolBefor: 0,
      memberDiscount: 0,
      total: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'Select Payment',
      userId: authStore.currentUser.id,
      user: authStore.currentUser,
      memberId: 0,
      member: undefined
    }
    memberStore.clear()
    dateStore.clear()
    currentStep.value = 1
    receipt.value.receivedAmount = 0
    receipt.value.change = 0
  }

  return {
    receiptItems,
    receipt,
    addReceiptItem,
    removeReceiptItem,
    inc,
    dec,
    calReceipt,
    clearDiscount,
    receiptDialog,
    showReceiptDialog,
    clear,
    changeTime,
    saveReceiptDataToStore,
    savedReceipts,
    updateCurrentMember,
    receiptDialogDetail,
    loadReceiptDetail,
    updatePaymentType,
    calReceivedAmount,
    saveReceipt,
    CheckSave,
    currentStep,
    nextStep,
    prevStep,
    updatePromotion,
    clearDate
  }
})
