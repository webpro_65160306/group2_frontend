import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
  const currentUser = ref<User>({
    id: 1,
    email: 'user1',
    password: 'Pass@Word123',
    fullName: 'Admin',
    gender: 'male',
    roles: ['admin', 'user'],
    timein: '',
    timeout: '',
    timework: 0
  })
  return { currentUser }
})
