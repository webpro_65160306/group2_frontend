
import type { ReceiptStock } from "@/types/ReceiptStock"
import type { StockItem } from "@/types/StockItem"
import type { Stock } from "@/types/Stock"
import { defineStore } from "pinia"
import { nextTick, provide, ref } from "vue"
import materialService from '@/services/material'
import material from "@/services/material"

export const useStockStore = defineStore('stock', () => {
    const updateDialog = ref(false)
    const historyDialog = ref(false)
    const showDetails = ref(false)
    const importDialog = ref(false)
    const checkDialog = ref(false)
    const receiptDialog = ref(false)
    const quantityIm = ref(1)

    const stock = ref<Stock[]>([
        { id: 1, name: 'กาแฟเมล็ด', MinQuantity: 10, unit: 'กิโลกรัม', quantityIm: 0, price: 200, quantityLasts: 6, quantityNow: 0 },
        { id: 2, name: 'นมสด', MinQuantity: 10, unit: 'ลิตร', quantityIm: 0, price: 70, quantityLasts: 10, quantityNow: 0 },
        { id: 3, name: 'น้ำตาล', MinQuantity: 50, unit: 'กิโลกรัม', quantityIm: 0, price: 30, quantityLasts: 66, quantityNow: 0 },
        { id: 4, name: 'น้ำเชื่อม', MinQuantity: 5, unit: 'ลิตร', quantityIm: 0, price: 40, quantityLasts: 0, quantityNow: 0 },
        { id: 5, name: 'ผงชาเขียว', MinQuantity: 5, unit: 'กิโลกรัม', quantityIm: 0, price: 40, quantityLasts: 10, quantityNow: 0 },
        { id: 6, name: 'ควิกเนสทีน', MinQuantity: 15, unit: 'กิโลกรัม', quantityIm: 0, price: 600, quantityLasts: 15, quantityNow: 0 },
        { id: 7, name: 'นมข้นหวาน', MinQuantity: 10, unit: 'กล่อง', quantityIm: 0, price: 150, quantityLasts: 11, quantityNow: 0 },
        { id: 8, name: 'ผงกาแฟ', MinQuantity: 5, unit: 'กิโลกรัม', quantityIm: 0, price: 30, quantityLasts: 3, quantityNow: 0 }
    ])

    const initialStock: Stock = {
        id: -1,
        name: '',
        quantityLasts: 0,
        quantityIm: 0,
        quantityNow: 0,
        MinQuantity: 0,
        price: 0,
        unit: ''
    }

    const editedStock = ref<Stock>(JSON.parse(JSON.stringify(initialStock)))
    const incOrdec = ref(false)
    const historyReceiptStock = ref<ReceiptStock[]>([])

    const receiptStock = ref<ReceiptStock>({
        id: 1,
        createdDate: new Date(),
        createdDateString: '',
        total: 0,
        change: 0,
        paymentType: 'Cash',
        receiptStockItem: []
    })
    const StockItem = ref<StockItem[]>([])

    provide('stock', stock)
    let lastId = 1
    async function addReceiptStock(stock: Stock) {
        const index = StockItem.value.findIndex((item) => item.stock?.id === stock.id)
        if (index >= 0) {
            cal()
            if (incOrdec.value === true) {
                if (StockItem.value[index].unit === 1) {
                    
                    // StockItem.value.splice(index, 1)
                    const res = await materialService.addMaterial(stock)
                } else {
                    StockItem.value[index].unit--
                }
                incOrdec.value = false
            } else {
                StockItem.value[index].unit++
            }
            cal()
            return
        } else {
            const newReceipt: StockItem = {
                id: lastId,
                name: stock.name,
                price: stock.price * stock.quantityIm,
                unit: stock.quantityIm,
                stockId: stock.id,
                stock: stock
            }
            StockItem.value.push(newReceipt)
            cal()
            lastId++
        }
    }

    function changeTime() {
        if (receiptStock.value.createdDate != undefined)
            receiptStock.value.createdDateString = receiptStock.value.createdDate.toLocaleString('en-US', {
                day: 'numeric',
                month: 'numeric',
                year: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                hour12: false
            });
    }

    function cal() {
        let total = 0
        for (const item of StockItem.value) {
            total = total + (item.price * item.unit)
        } receiptStock.value.total = total
    }

    function showImportDialog() {
        importDialog.value = true
    }
    function showCheckStock() {
        checkDialog.value = true
    }

    function showDetail(id: number) {
        const selectedReceiptHistory = historyReceiptStock.value.find((receipt) => receipt.id === id)

        if (selectedReceiptHistory) {
            const selected: ReceiptStock = {
                id: selectedReceiptHistory.id,
                createdDateString: selectedReceiptHistory.createdDateString,
                total: selectedReceiptHistory.total,
                change: selectedReceiptHistory.change,
                paymentType: selectedReceiptHistory.paymentType,
                receiptStockItem: selectedReceiptHistory.receiptStockItem
            }
            receiptStock.value = selected
            showDetails.value = true
        }
    }

    async function inc(item: Stock) {
        item.quantityIm++
        addReceiptStock(item)
        const res = await material.addMaterial(item)
    }
    async function dec(item: Stock) {
        if (item.quantityIm === 0) return
        incOrdec.value = true
        item.quantityIm--
        addReceiptStock(item)
        const res = await material.addMaterial(item)
    }

    function addHistory() {
        const receipts = { ...receiptStock.value }

        if (historyReceiptStock.value.length === 0) {
            lastId = 1
        } else {
            lastId = historyReceiptStock.value.length
            lastId++
        }
        receipts.id = lastId
        historyReceiptStock.value.push(receipts)
    }


    function clear() {
        StockItem.value = []
        receiptStock.value = {
            id: 0,
            createdDate: new Date(),
            createdDateString: '',
            total: 0,
            change: 0,
            paymentType: 'Cash',
            receiptStockItem: []
        }
        lastId = 1
    }

    const All = ref(true)
    const Almost = ref(false)
    const Out = ref(false)

    const tableStock = ref<Stock[]>([])
    function changeTable() {
        if (Almost.value === true) {
            console.log('Almost')
            All.value = false
            Out.value = false
            tableStock.value = stock.value.filter(item => item.quantityLasts < item.MinQuantity && item.quantityLasts > 0)
            nextTick(() => {
                Almost.value = false
            })
        } else if (Out.value === true) {
            Almost.value = false
            All.value = false
            tableStock.value = stock.value.filter(item => item.quantityLasts <= 0)
            nextTick(() => {
                Out.value = false
            })
        } else if (All.value === true) {
            Out.value = false
            Almost.value = false
            tableStock.value = stock.value
            nextTick(() => {
                All.value = false
            })
        }
    }

    function countAlmostStock() {
        let almostOutOfStock = 0
        stock.value.forEach((item, index) => {
            if (stock.value[index].quantityLasts < stock.value[index].MinQuantity && stock.value[index].quantityLasts != 0) {
                almostOutOfStock++
            }
        });
        return almostOutOfStock;
    }

    function countOutOfStock() {
        let countOutOfStock = 0
        stock.value.forEach((item, index) => {
            if (stock.value[index].quantityLasts === 0) {
                countOutOfStock++
            }
        });
        return countOutOfStock;
    }

    return {
        importDialog, quantityIm, checkDialog, stock, receiptStock, StockItem, historyDialog, updateDialog, editedStock, historyReceiptStock, showDetails, receiptDialog, All, Almost, Out, tableStock,
        showImportDialog, dec, inc, showCheckStock, addReceiptStock, cal, clear, changeTime, addHistory, showDetail, changeTable, countAlmostStock, countOutOfStock
    }
})