import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'

export const useProductStore = defineStore('product', () => {
  const currentProduct1 = ref<Product[]>([
    { id: 1, name: 'Espresso', price: 50.0, type: 1 },
    { id: 2, name: 'Americano', price: 40.0, type: 1 },
    { id: 3, name: 'Pumpkin & Pepita Loaf', price: 50.0, type: 2 },
    { id: 4, name: 'Butter Cake', price: 60.0, type: 2 },
    { id: 5, name: 'Cappuccino', price: 50.0, type: 1 },
    { id: 6, name: 'Green Tea', price: 55.0, type: 1 },
    { id: 7, name: 'Latté', price: 45.0, type: 1 },
    { id: 8, name: 'Oat Milk Cappuccino', price: 55.0, type: 1 },
    { id: 9, name: 'Mocha', price: 55.0, type: 1 },
    { id: 10, name: 'Muffin', price: 50.0, type: 2 },
    { id: 11, name: 'Baked Apple Croissant', price: 50.0, type: 2 },
    { id: 12, name: 'Butter Croissant', price: 55.0, type: 2 },
    { id: 13, name: 'Ham Croissant', price: 60.0, type: 2 },
    { id: 14, name: 'Cheese Danish', price: 60.0, type: 2 },
    { id: 15, name: 'Blueberry Muffin', price: 45.0, type: 2 },
    { id: 16, name: 'Chocolate Chip Cookie', price: 55.0, type: 2 },
    { id: 17, name: 'Plain Bagel', price: 50.0, type: 2 },
    { id: 18, name: 'Caramel milk', price: 40.0, type: 1 },
    { id: 19, name: 'Pineapple', price: 45.0, type: 1 },
    { id: 20, name: 'Thai tea', price: 55.0, type: 1 },
    { id: 21, name: 'Glazed Doughnut', price: 45.0, type: 2 },
    { id: 22, name: 'Honey lemon', price: 55.0, type: 1 },
    { id: 23, name: 'Peach Tea', price: 45.0, type: 1 },
    { id: 24, name: 'Black tea', price: 45.0, type: 1 },
    { id: 25, name: 'Jasmine tea', price: 55.0, type: 1 },
    { id: 26, name: 'Pink milk', price: 60.0, type: 1 },
    { id: 27, name: 'Taro milk', price: 50.0, type: 1 },
    { id: 28, name: 'Peach Strawberry ', price: 45.0, type: 1 },
    { id: 29, name: 'Mango Dragonfruit', price: 50.0, type: 1 },
    { id: 30, name: 'Milo', price: 55.0, type: 1 }
  ])

  const currentProduct2 = ref<Product[]>([
    { id: 1, name: 'Espresso', price: 50.0, type: 1 },
    { id: 2, name: 'Americano', price: 40.0, type: 1 },
    { id: 5, name: 'Cappuccino', price: 50.0, type: 1 },
    { id: 6, name: 'Green Tea', price: 55.0, type: 1 },
    { id: 7, name: 'Latté', price: 45.0, type: 1 },
    { id: 8, name: 'Oat Milk Cappuccino', price: 55.0, type: 1 },
    { id: 9, name: 'Mocha', price: 55.0, type: 1 },
    { id: 18, name: 'Caramel milk', price: 40.0, type: 1 },
    { id: 19, name: 'Pineapple', price: 45.0, type: 1 },
    { id: 20, name: 'Thai tea', price: 55.0, type: 1 },
    { id: 22, name: 'Honey lemon', price: 55.0, type: 1 },
    { id: 23, name: 'Peach Tea', price: 45.0, type: 1 },
    { id: 24, name: 'Black tea', price: 45.0, type: 1 },
    { id: 25, name: 'Jasmine tea', price: 55.0, type: 1 },
    { id: 26, name: 'Pink milk', price: 60.0, type: 1 },
    { id: 27, name: 'Taro milk', price: 50.0, type: 1 },
    { id: 28, name: 'Peach Strawberry ', price: 45.0, type: 1 },
    { id: 29, name: 'Mango Dragonfruit', price: 50.0, type: 1 },
    { id: 30, name: 'Milo', price: 55.0, type: 1 }
  ])

  const currentProduct3 = ref<Product[]>([
    { id: 3, name: 'Pumpkin & Pepita Loaf', price: 50.0, type: 2 },
    { id: 4, name: 'Butter Cake', price: 60.0, type: 2 },
    { id: 10, name: 'Muffin', price: 50.0, type: 2 },
    { id: 11, name: 'Baked Apple Croissant', price: 50.0, type: 2 },
    { id: 12, name: 'Butter Croissant', price: 55.0, type: 2 },
    { id: 13, name: 'Ham Croissant', price: 60.0, type: 2 },
    { id: 14, name: 'Cheese Danish', price: 60.0, type: 2 },
    { id: 15, name: 'Blueberry Muffin', price: 45.0, type: 2 },
    { id: 16, name: 'Chocolate Chip Cookie', price: 55.0, type: 2 },
    { id: 17, name: 'Plain Bagel', price: 50.0, type: 2 },
    { id: 21, name: 'Glazed Doughnut', price: 45.0, type: 2 }
  ])

  return { currentProduct1, currentProduct2, currentProduct3 }
})
