import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
  const memberDialog = ref(false)
  const members = ref<Member[]>([
    { id: 1, name: 'New Yasuo', tel: '0887979156' },
    { id: 2, name: 'Tew Jojo', tel: '0887679156' },
    { id: 2, name: 'Chanon WeedVan', tel: '0979697726' }
  ])
  const currentMember = ref<Member | null>()

  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
  }

  function clear() {
    currentMember.value = null
  }

  const addMember = (newMember: Member) => {
    members.value.push(newMember)
    memberDialog.value = false
  }

  return { addMember, memberDialog, members, searchMember, currentMember, clear }
})
